$(document).ready(function(){
	
	function initialize(){
		
		var myCenter=new google.maps.LatLng(54.683768, 25.279318);
		
		var mapOpt = {
			center:myCenter,
			zoom:6,
			disableDefaultUI:true, 
			mapTypeId:google.maps.MapTypeId.ROADMAP
		};
		
		var map=new google.maps.Map(document.getElementById("googleMap"),mapOpt);

		var marker=new google.maps.Marker({
			position:myCenter,
			map:map,
			draggable:true,
			icon:'include/img/mark.png'
		});
			
		$('.address-style').bind('blur',function(){
			geocoder = new google.maps.Geocoder();
			address = $(this).val();
			marker.setMap(null);

			geocoder.geocode( { 'address': address}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					map.setCenter(results[0].geometry.location);

					var marker = new google.maps.Marker({
						map: map, 
						position: results[0].geometry.location,
						draggable:true,
						icon:'include/img/mark.png'
					});
				} else {
					//alert("Tokia gatvė nebuvo rasta");
				}
			});
				
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
});

