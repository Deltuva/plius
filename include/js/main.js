
function setMarke(){
    $(".model_options").remove();
    var mark = $("#cl-marke").attr('value');
    var mark_name = $('option:selected', "#cl-marke").text();
    var list = $('#cl-modelis-list');

    if(mark){ // jeigu pasirinkta viena marke
        $('option[value=' + mark_name + ']',"#cl-marke").attr('selected', 'selected');
        $('#cl-modelis-list').removeAttr('disabled');
        for(model in modelArray[mark]){
            var modelId = modelArray[mark][model][0];
            var modelName = modelArray[mark][model][1];

            list.append('<option class="model_options" value="' + modelName + '" '
            + (selectedModel == modelId ? 'selected="selected" >' : ' >')
            + modelName + "</option>\n");
        }
        $("#cl-marke-opt").attr('label', ''+ mark_name +'');
        selectedModel = null;
    }

    if(!mark){ //jeigu nepasirinkta neleidziam pasirinkt
        $('#cl-modelis-list').attr('disabled', 'disabled');
    }
}

function Miestai()
{
    var miestai = new Array("Vilnius","Kaunas","Klaipėda","Šiauliai","Panevėžys","Akmenė","Alytus","Anykščiai","Ariogala","Visa Lietuva");
    for(var i=0; i<miestai.length;i++)
    {
        if(miestai[i] == "Vilnius"){
            $(".cl-miestai").append("<label class='checkbox'>" +
            "<input type='checkbox' name='cl_miestas[]' value='"+miestai[i]+"' checked/><span></span>" +
            " "+miestai[i]+" </label>");
        } else {
            $(".cl-miestai").append("<label class='checkbox'>" +
            "<input type='checkbox' name='cl_miestas[]' value='"+miestai[i]+"'/><span></span> " +
            ""+miestai[i]+" </label>");
        }
    }
    return true;
}
