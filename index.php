<?php require_once('system/config.php'); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Pridėk skelbimą</title>
    <link rel="stylesheet" href="include/css/style.css" type="text/css">
    <meta name="description" content="Galimybė viešinti skelbimus.">
    <meta name="keywords" content="Auto , skelbimai">
    <meta name="author" content="Mindaugas Deltuva">
    <link rel="shortcut icon" href="include/img/favicon.png">
    <script type="text/JavaScript" src="include/js/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyAr_RXHTBMWa7I-VyRRDSUOXDDvsqwfhfk"></script>
    <script type="text/JavaScript" src="include/js/maps.js"></script>
    <script type="text/JavaScript" src="include/js/main.js"></script>
</head>
<body>
<?php
for ($i = 2016; $i < 1930; $i++) {
    echo $i.' => '.$i;
}
if (isset($_POST['save'])) {
    if (isset($_POST['cl_marke'])) {
        $marke = $_POST['cl_marke'];
    } else {
        $marke = '';
    }
    if (isset($_POST['cl_modelis'])) {
        $modelis = $_POST['cl_modelis'];
    } else {
        $modelis = '';
    }
    if (isset($_POST['cl_dureles'])) {
        $dureles = preg_replace("/[^0-9]/", "", $_POST['cl_dureles']);
    } else {
        $dureles = '';
    }
    if (isset($_POST['cl_adresas'])) {
        $adresas = $_POST['cl_adresas'];
    } else {
        $adresas = '';
    }
    if (isset($_POST['cl_miestas'])) {
        $miestas = $_POST['cl_miestas'];
    } else {
        $miestas = '';
    }
    if (isset($_POST['cl_komentaras'])) {
        $komentaras = $_POST['cl_komentaras'];
    } else {
        $komentaras = '';
    }

    $validatorius = new Plius($db);
    $marke_ch = $validatorius->marke($marke);
    $modelis_ch = $validatorius->modelis($modelis);
    $dureles_ch = $validatorius->dureles($dureles);
    $adresas_ch = $validatorius->adresas($adresas);
    $miestas_ch = $validatorius->miestas($miestas);
    $komentaras_ch = $validatorius->komentaras($komentaras);

    if ($marke_ch == '' && $modelis_ch == '' && $dureles_ch == '' && $adresas_ch ==
        '' && $miestas_ch == '' && $komentaras_ch == '') {
        try {
            $miestai = implode(',', $miestas);
            $add = $validatorius->Idetiskelbima($marke, $modelis, $dureles, $adresas, $miestai, $komentaras);

            if ($add) {
                ?>
                        <div class="alert good">
                            Dėkojame! Skelbimas pridėtas ir laukia patvirtinimo.
                        </div>
                        <script type="text/JavaScript" src="include/js/redirect.js"></script>
                    <?php
            } else {
                ?>
                        <div class="alert error">
                            Klaida! Pridėti nepavyko.
                        </div>
                        <script type="text/JavaScript" src="include/js/redirect.js"></script>
                    <?php
            }
        } catch (PDOException $e) {
            echo 'Kazkas blogai';
            exit;
        }
    } else {
        ?>
        <div class="alert error">
            Klaida! Palikote neužpildyta laukelį.
        </div>
    <?php
    }
}
?>
<?php if (isset($_POST['abort'])) {
    ?>
    <div class="alert error">
        Atšaukėte
    </div>
    <script type="text/JavaScript" src="include/js/redirect.js"></script>
<?php
}
?>
<div class="main">
    <div class="left">
        <h3>
            <a title="PRIDĖK SKELBIMĄ" href="#">PRIDĖK SKELBIMĄ</a>
        </h3>
        <div class="text">
            Įdėti skelbimą nieko nekainuoja.
        </div>
    </div>
    <script type="text/javascript">
        var selectedModel = null;
        var modelArray = new Array();
        modelArray[2] = [[ 1612, 'i30']];
        modelArray[4] = [[ 48, 'C230'],[ 108, 'E320'],[ 27, 'Sprinter'],[ 31, 'Vito']];
        modelArray[5] = [[ 1595, '500'],[ 1598, 'Bravo'],[ 1152, 'Doblo'],[ 1153, 'Ducato'],[ 1155, 'Fiorino'],[ 1655, 'Freemont'],[ 1603, 'Punto Evo'],[ 1630, 'Qubo'],[ 1169, 'Scudo']];
        modelArray[97] = [[ 259, 'C4']];
        modelArray[101] = [[ 349, 'Phaeton'],[ 357, 'Touareg']];
        modelArray[104] = [[ 1654, '114'],[ 1633, '1M Coupe'],[ 387, '318'],[ 388, '320'],[ 1645, '320 Gran turismo'],[ 392, '328'],[ 1664, '330xd'],[ 1641, '420'],[ 1659, '420 Gran Coupe'],[ 396, '520'],[ 401, '530'],[ 1650, '530xd'],[ 407, '630'],[ 1651, '730xd'],[ 1658, '740xd'],[ 419, '750'],[ 428, 'M5'],[ 429, 'M6'],[ 430, 'X1'],[ 432, 'X5'],[ 1663, 'X5 M50d'],[ 433, 'X6'],[ 1628, 'X6 M'],[ 439, 'Z8']];
        modelArray[107] = [[ 497, 'Land Cruiser']];
        modelArray[110] = [[ 553, 'Cayenne'],[ 554, 'Cayman']];
        modelArray[113] = [[ 579, 'Colt']];
        modelArray[117] = [[ 665, 'Octavia']];
        modelArray[120] = [[ 715, 'XF']];
        modelArray[121] = [[ 727, 'GranTurismo'],[ 1640, 'Quattroporte S']];
        modelArray[124] = [[ 1620, 'Clubman'],[ 749, 'Cooper'],[ 1619, 'Countryman'],[ 1662, 'John Cooper Works'],[ 751, 'One']];
        modelArray[126] = [[ 762, 'A4'],[ 764, 'A6']];
        modelArray[134] = [[ 901, 'S80']];
        modelArray[142] = [[ 1054, 'Sportage']];
        modelArray[154] = [[ 1261, 'H2']];
        modelArray[161] = [[ 1304, '159'],[ 1311, 'Giulietta']];
        modelArray[164] = [[ 1327, 'Cherokee'],[ 1330, 'Grand Cherokee']];
    </script>
    <div class="right">
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="post">
            <div class="what-for">
                <b>Skelbimą paskelbė: </b>
                <input type="radio" name="cl_paskelbe" value="1" id="cl_paskelbe_2" checked/> Fizinis
                asmuo
                <input type="radio" name="cl_paskelbe" value="2" id="cl_paskelbe_3"/> Įmonė
            </div>
            <div class="form">
                Skelbimą paskelbė radio butonai čia tik taip dėl bendro vaizdo.<br/><br/>
                <div class="marke">
                    Markė: <br/>
                    <select name="cl_marke" class="marke-style <?php echo (isset($_POST['save']) && $marke == '') ? 'missing': null; ?>" onChange="setMarke()" id="cl-marke">
                        <option value="">Markė</option>
                        <option value="161" >Alfa Romeo</option>
                        <option value="126" >Audi</option>
                        <option value="104" >BMW</option>
                        <option value="97" >Citroen</option>
                        <option value="5" >Fiat</option>
                        <option value="2" >Hyundai</option>
                        <option value="154" >Hummer</option>
                        <option value="120" >Jaguar</option>
                        <option value="164" >Jeep</option>
                        <option value="142" >Kia</option>
                        <option value="121" >Maserati</option>
                        <option value="4" >Mercedes-Benz</option>
                        <option value="124" >Mini</option>
                        <option value="113" >Mitsubishi</option>
                        <option value="110" >Porsche</option>
                        <option value="117" >Skoda</option>
                        <option value="107" >Toyota</option>
                        <option value="101" >Volkswagen</option>
                        <option value="134" >Volvo</option>
                    </select>
                </div>
                <div class="modelis">
                    Modelis: <br/>
                    <select name="cl_modelis" class="modelis-style <?php echo (isset($_POST['save']) && $modelis == '') ? 'missing': null; ?>" id="cl-modelis-list" disabled="disabled">
                        <optgroup id="cl-marke-opt" class="doors">
                           <option value="">-- Pasirinkite --</option>
                        </optgroup>
                    </select>
                </div>
                <div class="clear"></div>
                <br/>

                <div class="duris doors">
                    Kiek durelių? <br/>
                    <select name="cl_dureles" class="duris-style <?php echo (isset($_POST['save']) && $dureles == '') ? 'missing': null; ?>">
                        <option value="">--</option>
                        <option value="2">2</option>
                        <option value="4">4</option>
                    </select>
                </div>
                <div class="address">
                    Adresas: <br/>
                    <input type="text" name="cl_adresas" class="address-style <?php echo (isset($_POST['save']) && $adresas == '') ? 'missing': null; ?>" value=""/><br/>
                    <span>Pvz.: Žalgirio 110, Vilnius</span>
                </div>
                <div class="clear"></div>
                <span>Miestas:</span><br/>
                <div class="cl-miestai">
                    <script type="text/javascript" language="JavaScript">Miestai()</script>
                </div>
                    <div class="clear"></div>
                <br/>
                <span>Komentaras:</span><br/>
                <textarea name="cl_komentaras" class="info-style <?php echo (isset($_POST['save']) && $komentaras == '') ? 'missing': null; ?>"></textarea>
                    <input type="submit" name="save" class="submit-save" value="Pridėti"/>
                    <input type="submit" name="abort" class="submit-abort" value="Atšaukti"/>
            </div>
            <div class="maps">
                Pasitikslink adresą žemėlapyje:<br/>

                <div id="googleMap"></div>
            </div>
        </form>
    </div>
    <div class="clear"></div>
</div>
</body>
</html>
