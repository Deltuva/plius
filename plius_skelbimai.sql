-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2015 m. Lie 18 d. 02:02
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `plius_skelbimai`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `skelbimai`
--

CREATE TABLE IF NOT EXISTS `skelbimai` (
`id` int(11) NOT NULL,
  `cl_marke` text NOT NULL,
  `cl_modelis` text NOT NULL,
  `cl_dureles` int(1) NOT NULL,
  `cl_adresas` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `cl_miestas` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `cl_komentaras` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Sukurta duomenų kopija lentelei `skelbimai`
--

INSERT INTO `skelbimai` (`id`, `cl_marke`, `cl_modelis`, `cl_dureles`, `cl_adresas`, `cl_miestas`, `cl_komentaras`) VALUES
(17, 'BMW', '2 serija', 4, 'Jaunimo 00, Ukmerge', 'Vilnius', 'Parduodu BMW 2 serijos, kas susidomejot rasykit.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `skelbimai`
--
ALTER TABLE `skelbimai`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `skelbimai`
--
ALTER TABLE `skelbimai`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
