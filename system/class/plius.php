<?php
class plius
{
    private $db;

    //mysql connectas
    public function __construct($db)
    {
        $this->db = $db;
    }

    //markes tikrinimas
    public function marke($marke)
    {
        if($marke == '')
        {
            $marketr = 'yra klaidu';
        }
        else
        {
            $marketr = '';
        }
        if($marketr != '')//jei yra klaidu, pridedam raudona maza pasvirusi teksta isvedimui
        {
            $arr_rtr = array('<div class="error">',$marketr,'</div>','<br>');
            $show_error = implode('',$arr_rtr);
        }
        else
        {
            $show_error = '';
        }
        return $show_error;
    }

    //modelio tikrinimas
    public function modelis($modelis)
    {
        if($modelis == '')
        {
            $modelistr = 'yra klaidu';
        }
        else
        {
            $modelistr = '';
        }
        if($modelistr != '')//jei yra klaidu, pridedam raudona maza pasvirusi teksta isvedimui
        {
            $arr_rtr = array('<div class="error">',$modelistr,'</div>','<br>');
            $show_error = implode('',$arr_rtr);
        }
        else
        {
            $show_error = '';
        }
        return $show_error;
    }

    //dureliu tikrinimas
    public function dureles($dureles)
    {
        if($dureles == '')
        {
            $durelestr = 'yra klaidu';
        }
        else
        {
            $durelestr = '';
        }
        if($durelestr != '')//jei yra klaidu, pridedam raudona maza pasvirusi teksta isvedimui
        {
            $arr_rtr = array('<div class="error">',$durelestr,'</div>','<br>');
            $show_error = implode('',$arr_rtr);
        }
        else
        {
            $show_error = '';
        }
        return $show_error;
    }

    //adreso tikrinimas
    public function adresas($adresas)
    {
        if($adresas == '')
        {
            $adresastr = 'yra klaidu';
        }
        else
        {
            $adresastr = '';
        }
        if($adresastr != '')//jei yra klaidu, pridedam raudona maza pasvirusi teksta isvedimui
        {
            $arr_rtr = array('<div class="error">',$adresastr,'</div>','<br>');
            $show_error = implode('',$arr_rtr);
        }
        else
        {
            $show_error = '';
        }
        return $show_error;
    }

    //miesto tikrinimas
    public function miestas($miestas)
    {
        if($miestas == '')
        {
            $miestastr = 'yra klaidu';
        }
        else
        {
            $miestastr = '';
        }
        if($miestastr != '')//jei yra klaidu, pridedam raudona maza pasvirusi teksta isvedimui
        {
            $arr_rtr = array('<div class="error">',$miestastr,'</div>','<br>');
            $show_error = implode('',$arr_rtr);
        }
        else
        {
            $show_error = '';
        }
        return $show_error;
    }

    //komentaro tikrinimas
    public function komentaras($komentaras)
    {
        if($komentaras == '')
        {
            $komentarastr = 'yra klaidu';
        }
        else
        {
            $komentarastr = '';
        }
        if($komentarastr != '')//jei yra klaidu, pridedam raudona maza pasvirusi teksta isvedimui
        {
            $arr_rtr = array('<div class="error">',$komentarastr,'</div>','<br>');
            $show_error = implode('',$arr_rtr);
        }
        else
        {
            $show_error = '';
        }
        return $show_error;
    }

    //skelbimo idejimo funkcija
    public function Idetiskelbima($marke,$modelis,$dureles,$adresas,$miestas,$komentaras)
    {
        $add = $this->db->prepare("
                    INSERT INTO skelbimai
                    SET cl_marke= :marke,cl_modelis= :modelis,cl_dureles= :dureles,cl_adresas= :adresas,cl_miestas= :miestas,cl_komentaras= :komentaras
                    ");
        $add->bindParam(':marke', $marke, PDO::PARAM_STR);
        $add->bindParam(':modelis', $modelis, PDO::PARAM_STR);
        $add->bindParam(':dureles', $dureles, PDO::PARAM_INT);
        $add->bindParam(':adresas', $adresas, PDO::PARAM_STR);
        $add->bindParam(':miestas', $miestas, PDO::PARAM_STR);
        $add->bindParam(':komentaras', $komentaras, PDO::PARAM_STR);
        $add->execute();

        return $add;
    }


}
